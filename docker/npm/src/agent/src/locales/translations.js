module.exports = {
    "Dashboard": {
        ru: "Панель управления",
        ua: "Панель управления"
    },
    "Logout": {
        ru: "Выйти",
        ua: "Выйти"
    },
    "Create new scratch sukuk bonds or other assets": {
        en: "Create a new scratch sukuk-bonds or other assets",
		ru: "Создать новые скретч сукук облигации или другие активы",
        ua: "Создать новые скретч-карты"
    },
    "Amount of scratch": {
        en: "Amount of scratch",
		ru: "Количество скретч",
        ua: "Количество карточек"
    },
	"Assets": {
        en: "Assets",
		ru: "Активы",
        ua: "Количество карточек"
	},	
    "Value of a scratch asset": {
        en: "Value of an asset",
		ru: "Стоимость актива",
        ua: "Стоимость карты"
    },
    "Total amount": {
        ru: "Общая сумма",
        ua: "Общая сумма"
    },
    "Create": {
        ru: "Создать",
        ua: "Создать"
    },
    "This page allows to create scratch sukuk bonds and other assets that can be distributed physically": {
        en: "This page allows to create scratch sukuk bonds and other assets that can be distributed physically",
		ru: "Эта страница позволяет создавать скретч сукук облигации и другие активы, которые потом могут быть распечатаны для последующего распространения",
        ua: "Эта страница позволяет создавать предоплаченные карты, которые потом могут быть распечатаны для последующего распространения"
    },
    "Scratch assets": {
        'en': "Scratch assets",
		'ru': "Скретч активы",
        'ua': "Карты пополнения"
    },
    "Created scratch assets": {
        en: "Sukuk-bonds",
		ru: "Созданные скретч активы",
        ua: "Картки поповнення"
    },
    "Amount": {
        ru: "Сумма",
        ua: "Сума"
    },
    "Asset": {
        en: "Asset",
		ru: "Актив",
        ua: "Валюта"
    },
    "Account": {
        ru: "Счет",
        ua: "Рахунок"
    },
    "Date of creation": {
        ru: "Дата создания",
        ua: "Дата створення"
    },
    "Date of usage": {
        ru: "Дата использования",
        ua: "Дата використання"
    },
    "Info": {
        en: "Info", 
		ru: "Информация",
        ua: "Інформація про картку"
    },
    "Show QR": {
        ru: "Показать QR-код",
        ua: "Показати QR-код"
    },
    "Card data": {
        ru: "Данные по карте пополнения",
        ua: "Дані по картці поповнення"
    },
    "Create new": {
        ru: "Создать новую",
        ua: "Створити нову"
    },
    "There is no cards created yet": {
        en: "No created scratch assets",
        ru: "Нет созданных скретч активов",
        ua: "Нет созданных скретч активов"
    },
    "Scratch asset account": {
        en: "Scratch asset account",
		ru: "Счет скретч актива",
        ua: "Рахунок картки поповнення"
    },
    "Max cards at time": {
        ru: "Максимально карт за один раз",
        ua: "Максимально карток за один раз"
    },
    "Check amount of scratch asset parameter": {
        ru: "Проверьте количество скретч актиаа ",
        ua: "Вийти"
    },
    "Not enough balance": {
        ru: "Недостаточно средств",
        ua: "Недостатньо грошей"
    },
    "Success. Cards will be confirmed in few minutes": {
        en: "Success. Scratch assets will be confirmed in few minutes",
        ru: "Успешно. Скретч-активы будут подтверждены через несколько минут",
        ua: "Успешно. Скретч-активы будут подтверждены через несколько минут"
    },
    "Agent balances": {
        ru: "Балансы агента",
        ua: "Баланси агента"
    },
    "Login/password combination is invalid": {
        ru: "Неправильный логин или пароль",
        ua: "Невірний логін або пароль"
    },
    "Depository panel": {
        en: "Depository panel",
		ru: "Панель депозитария",
        ua: "Панель агента"
    },
    "Username": {
        ru: "Логин",
        ua: "Логін"
    },
    "Password": {
        ru: "Пароль",
        ua: "Пароль"
    },
    "Log in": {
        ru: "Войти",
        ua: "Увійти"
    },
    "Prev": {
        ru: "Предыдущая",
        ua: "Попередня"
    },
    "Next": {
        ru: "Следующая",
        ua: "Наступна"
    },
    "Agent": {
        ru: "Агент",
        ua: "Агент"
    },
    "Time before the session close": {
        ru: "Время до закрытия сессии",
        ua: "Час до зачинення сессії"
    },
    "Scratch assets": {
        en: "Scratch assets",
		ru: "Скретч активы",
        ua: "Картки поповнення"
    },
    "Create scratch sukuk": {
        en: "Create scratch sukuk",
		ru: "Создать скретч сукук",
        ua: "Створити картки поповнення"
    },
    "Wait for data loading": {
        ru: "Ожидание загрузки",
        ua: "Очікування завантаження"
    },
    "en": {
        ru: 'ru',
        ua: 'ua'
    },

    //errors global
    "Error": {
        ru: 'Ошибка',
        ua: 'Помилка'
    },

    //error messages
    "Record not found": {
        ru: 'Не найдено',
        ua: 'Не знайдено'
    },
    "Transfer assets": {
        en: 'Transfer assets',
		ru: 'Перевод активов',
        ua: 'Переказ грошей'
    },
    "Transfer type": {
        ru: 'Идентификатор перевода',
        ua: 'Ідентифікатор переказу'
    },
    "Account ID": {
        ru: 'Номер счета',
        ua: 'Номер рахунку'
    },
    "Phone number": {
        ru: 'Номер телефона',
        ua: 'Номер телефону'
    },
    "Email": {
        ru: 'Электронная почта',
        ua: 'Електронна пошта'
    },
    "Asset": {
        ru: 'Актив',
        ua: 'Валюта'
    },
    "Invoice code": {
        ru: 'Номер инвойса',
        ua: 'Номер інвойсу'
    },
    "Request": {
        ru: 'Получить',
        ua: 'Отримати'
    },
    "Pay by invoice code": {
        ru: 'Осуществить платеж по инвойсу',
        ua: 'Здійснити платіж по інвойсу'
    },
    "by account ID": {
        ru: 'По номеру счета',
        ua: 'За номером рахунку'
    },
    "by phone": {
        ru: 'По номеру телефона',
        ua: 'За номером телефону'
    },
    "by email": {
        ru: 'По электронной почте',
        ua: 'За електронною поштою'
    },
    "Settings": {
        ru: 'Настройки',
        ua: 'Налаштування'
    },
    "New password cannot be same as old": {
        'ru': "Новый пароль не должен быть таким же, как и предыдущий",
        'ua': "Новий пароль не повинен бути таким самим, як і попередній"
    },
    "Old password": {
        ru: 'Предыдущий пароль',
        ua: 'Попередній пароль'
    },
    "New password": {
        ru: 'Новый пароль',
        ua: 'Новий пароль'
    },
    "Repeat new password": {
        ru: 'Подвердите новый пароль',
        ua: 'Підтвердіть овий пароль'
    },
    "Change account info": {
        ru: 'Изменить информацию об учетной записи',
        ua: 'Змінити інформацію про обліковий запис'
    },
    "Change": {
        ru: 'Изменить',
        ua: 'Змінити'
    },
    "Phone": {
        ru: 'Номер телефона',
        ua: 'Номер телефону'
    },
    "Save": {
        ru: 'Сохранить',
        ua: 'Зберегти'
    },
    "Account transactions": {
        ru: 'Список транзакций',
        ua: 'Список трансакцій'
    },
    "Overview of recent transactions": {
        ru: 'Обзор последних транзакций',
        ua: 'Список останніх трансакцій'
    },
    "Type": {
        ru: 'Тип',
        ua: 'Тип'
    },
   "Credit": {
        ru: 'Кредит',
        ua: 'Кредит'
    },
    "Debit": {
        ru: 'Дебет',
        ua: 'Дебет'
    },
    "Date": {
        ru: 'Дата',
        ua: 'Дата'
    },
    "Account id": {
        ru: 'Номер счета',
        ua: 'Номер рахунку'
    },
    "Your QRCode": {
        ru: 'QR-код',
        ua: 'QR-код'
    },
    "Balances": {
        ru: 'Баланс',
        ua: 'Баланс'
    },
    "Invoice requested": {
        ru: "Инвойс запрошен",
        ua: "Інвойс запрошений"
    },
    "Show account": {
        ru: 'Показать номер счета',
        ua: 'Показати номер рахунку'
    },
    "Your account": {
        ru: 'Номер счета',
        ua: 'Номер рахунку'
    },
    "Payments": {
        ru: 'Платежи',
        ua: 'Платежі'
    },
    "Forgot your password?": {
        ru: 'Забыли пароль?',
        ua: 'Забули пароль?'
    },
    "Change password": {
        ru: 'Изменить пароль',
        ua: 'Змінити пароль'
    },
    "Password should have 6 chars min": {
        ru: 'Пароль должен содержать не менее из 6 симоволов',
        ua: 'Пароль повинен містити не менше 6 символів'
    },
    "Passwords should match": {
        ru: 'Пароли должны совпадать',
        ua: 'Паролі повинні співпадати'
    },
    "Cannot change password": {
        ru: 'Невозможно изменить пароль',
        ua: 'Неможливо змінити пароль'
    },
    "No payments yet": {
        ru: 'Нет проведенных платежей',
        ua: 'Немає проведених платежів'
    },
    "Create a new invoice": {
        ru: 'Создать новый инвойс',
        ua: 'Створити новий інвойс'
    },
    "Invoices": {
        ru: 'Инвойсы',
        ua: 'Інвойси'
    },
    "Settlement": {
        ru: 'Погашение',
        ua: 'Погашення'
    },
    "Make settlement": {
        ru: 'Сделать погашение',
        ua: 'Зробити погашення'
    },
    "Submit": {
        ru: 'Принять',
        ua: 'Прийняти'
    },
    "Copy this invoice code and share it with someone you need to get money from": {
        ru: 'Скопируйте этот инвойс и отправьте тому, от кого вы желаете получить платеж',
        ua: 'Скопіюйте цей інвойс і надішліть тому, від кого ви бажаєте отримати платіж'
    },
    "Transfer successful": {
        ru: 'Платеж выполнен',
        ua: 'Платіж виконаний'
    },
    "Transfer": {
        ru: 'Перевод',
        ua: 'Переказ'
    },
    "Success": {
        ru: 'Успех',
        ua: 'Успіх'
    },
    "Log in via mnemonic phrase": {
        ru: 'Войдите с помощью мнемонической фразы',
        ua: 'Увійдіть за допомогою мнемонічной фрази'
    },
    "Enter your mnemonic phrase word number $[1] of $[2]" : {
        'en': "Enter your mnemonic phrase word number $[1] of $[2].",
        'ru': "Введите Вашу мнемоническую фразу. Слово №$[1] из $[2].",
        'ua': "Введіть Вашу мнемонічну фразу. Слово №$[1] із $[2]."
    },
    "Mnemonic phrase" : {
        'ru': "Мнемоническая фраза",
        'ua': "Мнемонічна фраза"
    },
    "Bad account id": {
        ru: 'Неверный номер счета',
        ua: 'Хибний номер рахунку'
    },
    "Can not make settlement": {
        ru: 'Невозможно выполнить погашение',
        ua: 'Неможливо виконати погешення'
    },
    "Account is invalid": {
        'ru': "Неверный номер счета",
        'ua': "Невірний номер рахунку"
    },
    "Show older" :{
        'ru': "Показать больше",
        'ua': "Показать больше",
    },
    "Success. Success. Scratch assets will be confirmed in few moments" :{
        'en': "Success. Scratch assets will be confirmed in few minutes",
        'ru': "Успешно. Скретч активы будут подтверждены через несколько минут",
        'ua': "Успешно. Скретч активы будут подтверждены через несколько минут"
    },
    //error descriptions
    "invoice": {
        ru: 'инвойс',
        ua: 'инвойс'
    },
    "Cannot make transfer": {
        ru: 'Невозможно осуществить перевод',
        ua: 'Невозможно осуществить перевод'
    },
    "Can not generate scratch assets": {
		en: 'Can not generate scratch assets',
		ru: 'Невозможно сгенерировать скретч активы',
        ua: 'Невозможно сгенерировать скретч активы'
	},	
};